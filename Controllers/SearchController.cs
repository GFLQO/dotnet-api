using System.Collections.Generic;
using AutoMapper;
using Commander.Data;
using Commander.Dtos;
using Commander.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace Commander.Controllers
{
    //api/commandsearch
    [Route("api/commandsearch")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly ICommanderRepo _repository;
        private readonly IMapper _mapper;

        public SearchController(ICommanderRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        
        [HttpGet]
         public ActionResult<IEnumerable<CommandReadDto>> GetAllCommands()
        {
            var commandItems = _repository.GetAllCommands();
            return Ok(_mapper.Map<IEnumerable<CommandReadDto>>(commandItems));
        }
        
        //GET/api/commandsearch/{HowTo}
        [HttpGet("{HowTo}", Name = "GetCommandByName")]
        public ActionResult<IEnumerable<CommandReadDto>> GetCommandByName(string searchText)
        {
            
            var commandItem = _repository.GetCommandByName(searchText);
            if( commandItem != null)
            {
                return Ok(_mapper.Map<IEnumerable<CommandReadDto>>(commandItem));
            }
            return NotFound();
        }
    }
    
}